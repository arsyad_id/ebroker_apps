import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/credentials/LoginScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomHeader {

  static logout(context) async {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) {
          return LoginScreen();
        }), (Route<dynamic> route) => false);

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(Globals.isLoggedInKey);
    prefs.remove(Globals.idUserKey);
    prefs.remove(Globals.levelKey);
    prefs.remove(Globals.authKey);
    prefs.remove(Globals.emailKey);
  }

  static Widget getHeaderRental(context, level, email) {
    return SafeArea(
        child: Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width,
          color: Colors.green,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset('assets/icon-user-rental2.png',height: 40,width: 40,),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Hi, "+level, style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                        ),),
                        Text(email,style: TextStyle(
                            color: Colors.white,
                            fontSize: 16
                        ))
                      ],
                    ),
                  ),
                ],
              ),
              IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: () {
                    logout(context);
                  })
            ],
          ),
        ),
    );
  }

  static Widget getHeaderPetani(context, level, email) {
    return SafeArea(
        child: Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width,
          color: Colors.lightBlue,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset('assets/icon-user-petani2.png',height: 40,width: 40,),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Hi, "+level, style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                        ),),
                        Text(email,style: TextStyle(
                            color: Colors.white,
                            fontSize: 16
                        ))
                      ],
                    ),
                  ),
                ],
              ),
              IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: () {
                    logout(context);
                  })
            ],
          ),
        ),
    );
  }
}
