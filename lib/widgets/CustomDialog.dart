import 'package:flutter/material.dart';

class CustomDialog {
  static Future getDialog(String title, String message, BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message, style: TextStyle(color: Colors.black),),
            actions: <Widget>[
              FlatButton(
                child: Text("Tutup"),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
  static Future getDialogDoublePop(String title, String message, BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message, style: TextStyle(color: Colors.black),),
            actions: <Widget>[
              FlatButton(
                child: Text("Tutup"),
                onPressed: (){
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}