import 'package:dio/dio.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';

class Globals{
  static final String urlRoot = "https://e-broker.yehezkieldeta.xyz/";

  static final String urlApi = urlRoot + "api/";
  static final String urlTransaction = urlRoot + "transaksi/";
  static final String urlRental = urlRoot + "rental/";


  static Dio dio;
  static final String isLoggedInKey = "IS_LOGGED_IN";
  static final String idUserKey = "ID_USER";
  static final String levelKey = "LEVEL";
  static final String authKey = "AUTH";
  static final String latKey = "LAT";
  static final String longKey = "LONG";
  static final String fuzzyKey = "FUZZY";
  static final String emailKey = "EMAIL";
  static final String apiMapKey = "pk.eyJ1IjoiYXJzeWFkLWlkbiIsImEiOiJja2l2ZGQ3eDkwMGM3MnltNjY4MjlqOW1sIn0.yQRxVTZOVAIhh7dFxFxlsQ";
  static final double latTugu = -7.246135286687945;
  static final double longTugu = 112.73784886386392;
}