import 'dart:async';

import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/MenuScreen.dart';
import 'package:e_broker/views/credentials/LoginScreen.dart';
import 'package:e_broker/views/petani/HomeScreen.dart';
import 'package:e_broker/views/rental/RentalHomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String levelKey;

  startSplashScreen() async {
    var duration = const Duration(seconds: 2);
    return Timer(duration, () async {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      bool isLoggedIn = false;
      levelKey = prefs.getString(Globals.levelKey);
      prefs.getBool(Globals.isLoggedInKey) == null
          ? isLoggedIn = false
          : isLoggedIn = true;
      print("Login Status : " + isLoggedIn.toString());
      if (isLoggedIn) {
        if (levelKey == "PETANI") {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_) {
              return HomeScreen();
            }),
          );
        } else {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_) {
              return RentalHomeScreen();
            }),
          );
        }
      } else {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) {
            return LoginScreen();
          }),
        );
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startSplashScreen();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Text(
                "Welcome to",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20),
              ),
            ),
            Container(
              child: Text(
                "E-BROKER",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 32),
              ),
            ),
            Container(
              child: Image.asset("assets/TA logo-01.png"),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: Text(
                "Application For Transporting Agricultural Products Towards Smart Rural Communities.",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 18, fontStyle: FontStyle.italic),
              ),
            )
          ],
        ),
      ),
    );
  }
}
