import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/petani/HomeScreen.dart';
import 'package:e_broker/views/rental/RentalHomeScreen.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'RegisterScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _email = new TextEditingController();
  final TextEditingController _password = new TextEditingController();

  FormData formData;

  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void postLogin() async {
    try {
      setState(() {
        isLoading = true;
      });
      if (_email.text != "" && _password.text != "") {
        var url = Globals.urlRoot + "login";
        Map<String, dynamic> param = new Map();
        param['email'] = _email.text;
        param['password'] = _password.text;
        var jsonParam = json.encode(param);
        Response response = await Dio().post(url, data: jsonParam);
        print(response.data);
        final data = response.data;
        if (data['data'] != null) {
          print("OK");
          final SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool(Globals.isLoggedInKey, true);
          prefs.setInt(Globals.idUserKey, data['data']['user']['id']);
          prefs.setString(Globals.authKey, data['data']['token']);
          prefs.setString(Globals.emailKey, data['data']['user']['email']);
          prefs.setString(Globals.levelKey, data['data']['user']['level']);
          if (data['data']['user']['level'] == "PETANI"){
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
              return HomeScreen();
            }));
          }else{
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
              return RentalHomeScreen();
            }));
          }
        }
        setState(() {
          isLoading = false;
        });
      } else {
        CustomDialog.getDialog(
            "Peringatan", "Harap isi field dengan lengkap.", context);
      }
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(e.response.statusMessage,
          e.response.data['message'].toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.blue,
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Container(
                child: Center(
                    child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        child: Text(
                          "E-BROKER",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.4,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        margin: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.all(10),
                              child: TextField(
                                controller: _email,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.alternate_email),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    labelText: "E-Mail"),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(10),
                              child: TextField(
                                controller: _password,
                                obscureText: true,
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.lock),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    labelText: "Password"),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                  onPressed: () async {
                                    postLogin();
                                  },
                                  child: Text(
                                    "LOGIN",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  color: Colors.blue,
                                  shape: StadiumBorder()),
                            ),
                            Container(
                              child: OutlineButton(
                                  onPressed: () {
                                    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                                      return RegisterScreen();
                                    }));
                                  },
                                  child: Text(
                                    "REGISTER",
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  color: Colors.blue,
                                  borderSide: BorderSide(
                                      color: Colors.blue,
                                      style: BorderStyle.solid,
                                      width: 1),
                                  shape: StadiumBorder()),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
              ));
  }
}
