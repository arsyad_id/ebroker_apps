import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => new _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _email = new TextEditingController();
  final TextEditingController _password = new TextEditingController();
  final TextEditingController _name = new TextEditingController();
  final TextEditingController _address = new TextEditingController();
  final TextEditingController _truck = new TextEditingController();
  final TextEditingController _payload = new TextEditingController();
  final TextEditingController _machine = new TextEditingController();
  final TextEditingController _price = new TextEditingController();
  final TextEditingController _year = new TextEditingController();
  bool isRental = false;

  double lat,long;

  FormData formData;

  File photo;
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void getLocation()async{
    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    setState(() {
      lat = _locationData.latitude;
      long = _locationData.longitude;
    });
    print('Latitude: $lat, Longitude: $long');
  }

  Future<void> pickImage() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    );

    if(result != null) {
      setState(() {
        photo = File(result.files.single.path);
      });
    } else {
      // User canceled the picker
      CustomDialog.getDialog("Perhatian", "Jangan lupa untuk pilih foto yaa.", context);
    }
  }

  void registerPetani() async {
    try {
      setState(() {
        isLoading = true;
      });
      if (_email.text != "" && _password.text != "" && _name.text != "") {
        var url = Globals.urlRoot + "register/petani";
        Map<String, dynamic> param = new Map();
        param['email'] = _email.text;
        param['password'] = _password.text;
        param['nama'] = _name.text;
        var jsonParam = json.encode(param);
        Response response = await Dio().post(url, data: jsonParam);
        print(response.data);
        final data = response.data;
        if (data['data'] != null) {
          print("OK");
          CustomDialog.getDialogDoublePop(
              "Sukses", "Registrasi telah berhasil dilakukan.", context);
        }
        setState(() {
          isLoading = false;
        });
      } else {
        CustomDialog.getDialog(
            "Peringatan", "Harap isi field dengan lengkap.", context);
      }
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(e.response.statusMessage,
          e.response.data['message'].toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  void registerRental() async {
    try {
      setState(() {
        isLoading = true;
      });
      if (_email.text != "" &&
          _password.text != "" &&
          _name.text != "" &&
          _address.text != "" &&
          _truck.text != "" &&
          _payload.text != "" &&
          _machine.text != "" &&
          _price.text != "" &&
          _year.text != "") {
        var url = Globals.urlRoot + "register/rental";
         formData = new FormData.fromMap({
         'email' : _email.text,
         'password' : _password.text,
         'nama' : _name.text,
         'alamat' : _address.text,
         'jenis_truk' : _truck.text,
         'kapasitas_muatan' : _payload.text,
         'kapasitas_mesin' : _machine.text,
         'harga_sewa' : _price.text,
         'tahun' : _year.text,
         'long' : long.toString(),
         'lat' : lat.toString(),
           "foto": await MultipartFile.fromFile(photo.path),
        });
        // Map<String, dynamic> param = new Map();
        // param['email'] = _email.text;
        // param['password'] = _password.text;
        // param['nama'] = _name.text;
        // param['alamat'] = _address.text;
        // param['jenis_truk'] = _truck.text;
        // param['kapasitas_muatan'] = _payload.text;
        // param['kapasitas_mesin'] = _machine.text;
        // param['harga_sewa'] = _price.text;
        // param['tahun'] = _year.text;
        // param['long'] = long.toString();
        // param['lat'] = lat.toString();
        // param['foto'] = await MultipartFile.fromFile(photo.path);
        // var jsonParam = json.encode(param);
        Response response = await Dio().post(url, data: formData);
        print(response.data);
        final data = response.data;
        if (data['data'] != null) {
          print("OK");
          CustomDialog.getDialogDoublePop(
              "Sukses", "Registrasi telah berhasil dilakukan.", context);
        }
        setState(() {
          isLoading = false;
        });
      } else {
        CustomDialog.getDialog(
            "Peringatan", "Harap isi field dengan lengkap.", context);
      }
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(e.response.statusMessage,
          e.response.data['message'].toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.blue,
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Container(
                margin: EdgeInsets.only(top: 50),
                child: Column(
                  children: [
                    Container(
                      child: Text(
                        "REGISTER E-BROKER",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          RaisedButton.icon(
                              color: isRental ? Colors.white : Colors.grey,
                              shape: StadiumBorder(
                                  side: BorderSide(color: Colors.blue)),
                              onPressed: () {
                                setState(() {
                                  isRental = false;
                                });
                              },
                              icon: Icon(Icons.person),
                              label: Text("Petani")),
                          RaisedButton.icon(
                              color: isRental ? Colors.grey : Colors.white,
                              shape: StadiumBorder(
                                  side: BorderSide(color: Colors.blue)),
                              onPressed: () {
                                setState(() {
                                  isRental = true;
                                });
                                getLocation();
                              },
                              icon: Icon(Icons.drive_eta),
                              label: Text("Rental"))
                        ],
                      ),
                    ),
                    isRental ? buildFormRental() : buildFormPetani(),
                  ],
                ),
              )));
  }

  buildFormPetani() {
    return Center(
        child: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _email,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.alternate_email),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "E-Mail"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _password,
                    obscureText: true,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Password"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _name,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Nama"),
                  ),
                ),
                Container(
                  child: OutlineButton(
                      onPressed: () {
                        registerPetani();
                      },
                      child: Text(
                        "REGISTER",
                        style: TextStyle(
                            color: Colors.blue, fontWeight: FontWeight.bold),
                      ),
                      color: Colors.blue,
                      borderSide: BorderSide(
                          color: Colors.blue,
                          style: BorderStyle.solid,
                          width: 1),
                      shape: StadiumBorder()),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }

  buildFormRental() {
    return Center(
        child: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _email,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.alternate_email),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "E-Mail"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _password,
                    obscureText: true,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Password"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _name,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Nama"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _address,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.home_filled),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Alamat"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _truck,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.drive_eta),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Jenis Truck"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _payload,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.all_inbox),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Kapasitas Muatan"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _machine,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.storage),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Kapasitas Mesin"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _price,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.attach_money),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Harga Sewa"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TextField(
                    controller: _year,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.calendar_today),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Tahun"),
                  ),
                ),
                photo == null ?
                Container(
                  margin: EdgeInsets.all(10),
                  child: Icon(Icons.image, size: 128,)
                ):
                Container(
                    margin: EdgeInsets.all(10),
                    child: Image.file(photo,fit: BoxFit.fill,)
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: MaterialButton(
                    shape: StadiumBorder(),
                    color: Colors.blueGrey,
                    onPressed: pickImage,
                    child: Text("Pilih Foto",style: TextStyle(
                      color: Colors.white
                    ),),
                  )
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: OutlineButton(
                      onPressed: () {
                        registerRental();
                      },
                      child: Text(
                        "REGISTER",
                        style: TextStyle(
                            color: Colors.blue, fontWeight: FontWeight.bold),
                      ),
                      color: Colors.blue,
                      borderSide: BorderSide(
                          color: Colors.blue,
                          style: BorderStyle.solid,
                          width: 1),
                      shape: StadiumBorder()),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
