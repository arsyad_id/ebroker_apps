import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:e_broker/models/RentalModel.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/ViewRouteScreen.dart';
import 'package:e_broker/views/petani/ViewMapScreen.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:e_broker/widgets/CustomHeader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransactionScreen extends StatefulWidget {
  @override
  _TransactionScreenState createState() => new _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  List<List<double>> result = new List();
  List<RentalModel> listTransaction = new List();
  bool isLoading = false;
  String authKey, email, level;
  double _rating = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  void initData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    email = prefs.getString(Globals.emailKey);
    level = prefs.getString(Globals.levelKey);
    loadListTransaction();
  }

  void loadListTransaction() async {
    try {
      listTransaction.clear();
      var url = Globals.urlTransaction + "list";
      var options = Options(headers: {"Authorization": "Bearer " + authKey});
      Response response = await Dio().get(url, options: options);
      print(response.data);
      final data = response.data;
      if (data != null) {
        if (data['status'] == 200) {
          print("OK");
          List _dataResults = data['data'];
          print(_dataResults);
          _dataResults.forEach((element) {
            listTransaction.add(new RentalModel.listTransaction(
                element['id'],
                element['created_at'],
                element['updated_at'],
                element['id_petani'],
                element['id_rental'],
                element['status']));
          });
        } else {
          CustomDialog.getDialog("Peringatan", data['message'], context);
        }
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  void finishOrder(int _idTransaction, String _idRental) async {
    try {
      setState(() {
        isLoading = true;
      });
      var url = Globals.urlTransaction + "finish/" + _idTransaction.toString();
      var options = Options(headers: {"Authorization": "Bearer " + authKey});
      Response response = await Dio().get(url, options: options);
      print(response.data);
      final data = response.data;
      if (data['status'] == 200) {
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      print(e.response.data);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
    setRating(_idRental);
  }

  void setRating(String _idRental) async {
    try {
      if (_rating != null) {
        setState(() {
          isLoading = true;
        });
        var url = Globals.urlTransaction + "rating";
        Map<String, dynamic> param = new Map();
        param['id_rental'] = _idRental;
        param['rating'] = _rating;
        var jsonParam = json.encode(param);
        var options = Options(headers: {"Authorization": "Bearer " + authKey});
        Response response =
            await Dio().post(url, data: jsonParam, options: options);
        print(response.data);
        final data = response.data;
        if (data['status'] == 200) {
          CustomDialog.getDialog(
              data['message'], "Transaksi berhasil diselesaikan.", context);
        }
      } else {
        CustomDialog.getDialog(
            "Peringatan", "Harap isi rating minimal 1 bintang.", context);
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      print(e.response.data);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
    loadListTransaction();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : buildList());
  }

  buildList() {
    return Container(
        child: Column(
      children: [
        CustomHeader.getHeaderPetani(context, level, email),
        Expanded(
          child: ListView.builder(
              itemCount: listTransaction.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListView(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text((index+1).toString()+". " + "Status Pesanan"),
                              Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    color: listTransaction[index].status ==
                                            "Diproses"
                                        ? Colors.green
                                        : listTransaction[index].status ==
                                                "Menunggu"
                                            ? Colors.yellow
                                            : Colors.blue,
                                    borderRadius: BorderRadius.circular(15)),
                                child: Text(
                                  listTransaction[index].status,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ],
                          )),
                      Container(
                          margin: EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Pesanan dibuat"),
                              Text(DateTime.parse(listTransaction[index].createdAt).toString()),
                            ],
                          )),
                      listTransaction[index].status == "Diproses"
                          ? Container(
                              margin: EdgeInsets.only(left: 20, right: 20),
                              child: RaisedButton(
                                shape: StadiumBorder(),
                                color: Colors.blue,
                                // shape: StadiumBorder(),
                                onPressed: () {
                                  showAlertDialog(
                                      context, listTransaction[index].id, listTransaction[index].idRental);
                                },
                                child: Text(
                                  "TRANSAKSI SELESAI",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            )
                          : Container(),
                      Container(
                        margin: EdgeInsets.only(
                            left: 20, right: 20, bottom: 10),
                        child: MaterialButton(
                          color: Colors.teal,
                          shape: StadiumBorder(),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                              return ViewRouteScreen(idTransaction: listTransaction[index].id,isCreated: false,);
                            }));
                          },
                          child: Text(
                            "LIHAT RUTE",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                );
              }),
        ),
      ],
    ));
  }

  showAlertDialog(BuildContext context, int _id, String _idRental) {
    // set up the buttons
    Widget cancelButton = MaterialButton(
      child: Text(
        "TIDAK",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = MaterialButton(
      child: Text(
        "YA",
        style: TextStyle(color: Colors.blue),
      ),
      onPressed: () {
        finishOrder(_id, _idRental);
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Peringatan"),
      content: Container(
        height: 100,
        child: Column(
          children: [
            Text("Apakah Anda yakin ingin menyelesaikan transaksi ini?"),
            RatingBar.builder(
              initialRating: 1,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: false,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
                setState(() {
                  _rating = rating;
                });
              },
            ),
          ],
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showRatingBar(BuildContext context, String _id) {
    // set up the buttons
    Widget continueButton = MaterialButton(
      child: Text(
        "SUBMIT",
        style: TextStyle(color: Colors.blue),
      ),
      onPressed: () {
        // finishOrder(_id);
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Beri Rating"),
      content: RatingBar.builder(
        initialRating: 1,
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: false,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Colors.amber,
        ),
        onRatingUpdate: (rating) {
          print(rating);
        },
      ),
      actions: [
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
