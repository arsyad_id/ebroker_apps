import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/widgets/CustomHeader.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => new _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  bool isLoading = false;
  String authKey, level, email;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  initData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    email = prefs.getString(Globals.emailKey);
    level = prefs.getString(Globals.levelKey);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              child: Column(
                children: [
                  level == "PETANI"
                      ? CustomHeader.getHeaderPetani(context, level, email)
                      : CustomHeader.getHeaderRental(context, level, email),
                  Container(
                    margin: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Image.asset("assets/TA logo-01.png"),
                        Text(
                          "e-Broker Transportasi Hasil Pertanian Menuju Masyarakat Pedesaan Cerdas",
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          "Merupakan aplikasi android yang dirancang untuk memudahkan para petani Indonesia",
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          "dalam mengirim hasil pertaniannya menuju konsumen secara langsung.\n",
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          "Aplikasi ini dilengkapi fitur rekomendasi kendaraan serta rute yang akan dilalui selama pengiriman, sehingga membuat pemilihan kendaraan dan proses pengiriman yang dilakukan jauh lebih efektif. Dengan aplikasi ini diharapkan dapat mengingkatkan taraf hidup para petani khusus nya yang ada di pedesaaan dengan mendapat keuntungan yang lebih tinggi serta mendorong petani di pedesaan menuju masyarakat pedesaan cerdas.",
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
