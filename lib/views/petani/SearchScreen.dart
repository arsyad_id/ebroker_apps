import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:e_broker/models/FuzzyModel.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/petani/ResultsScreen.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:e_broker/widgets/CustomHeader.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => new _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<List<double>> result = new List();

  bool isLoading = false;
  bool isLoaded = false;
  String authKey;
  String level, email;

  List<FuzzyModel> _dropdownItems = [
    FuzzyModel([0.0, 0.1, 0.3], "Tidak Penting"),
    FuzzyModel([0.1, 0.3, 0.5], "Kurang Penting"),
    FuzzyModel([0.3, 0.5, 0.7], "Cukup Penting"),
    FuzzyModel([0.5, 0.7, 0.9], "Penting"),
    FuzzyModel([0.7, 0.9, 1.0], "Sangat Penting"),
  ];

  List<DropdownMenuItem<FuzzyModel>> _dropdownMenuItems;
  FuzzyModel _selectedItemSatu,
      _selectedItemDua,
      _selectedItemTiga,
      _selectedItemEmpat,
      _selectedItemLima;

  double lat, long;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dropdownMenuItems = buildDropDownMenuItems(_dropdownItems);
    _selectedItemSatu = _dropdownMenuItems[0].value;
    _selectedItemDua = _dropdownMenuItems[0].value;
    _selectedItemTiga = _dropdownMenuItems[0].value;
    _selectedItemEmpat = _dropdownMenuItems[0].value;
    _selectedItemLima = _dropdownMenuItems[0].value;
    initData();
  }

  List<DropdownMenuItem<FuzzyModel>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<FuzzyModel>> items = List();
    for (FuzzyModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.parameter),
          value: listItem,
        ),
      );
    }
    return items;
  }

  void initData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    email = prefs.getString(Globals.emailKey);
    level = prefs.getString(Globals.levelKey);
    print('Auth: $authKey');
    print('Level: $level');
    print('Email: $email');
    setState(() {
      isLoading = false;
    });
  }

  void getLocation() async {
    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    setState(() {
      lat = _locationData.latitude;
      long = _locationData.longitude;
    });
    print('Latitude: $lat, Longitude: $long');
    print('Auth: $authKey');
    setLocation();
  }

  void setLocation() async {
    try {
      if (lat != null && long != null) {
        setState(() {
          isLoading = true;
        });
        final SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setDouble(Globals.longKey, long);
        prefs.setDouble(Globals.latKey, lat);
        var url = Globals.urlRoot + "set-location";
        Map<String, dynamic> param = new Map();
        param['long'] = long.toString();
        param['lat'] = lat.toString();
        var jsonParam = json.encode(param);
        var options = Options(headers: {"Authorization": "Bearer " + authKey});
        Response response =
            await Dio().post(url, data: jsonParam, options: options);
        print(response.data);
        final data = response.data;
        if (data != null) {
          print("OK");
        }
      } else {
        CustomDialog.getDialog("Peringatan",
            "Tidak dapat menemukan posisi latitude & longitude.", context);
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      print(e.response.data);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Container(
                child: Center(
                  child: Column(
                    children: [
                      CustomHeader.getHeaderPetani(context, level, email),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Image.asset('assets/TA banner-01 biru.jpg'),
                      ),
                      Visibility(
                        visible: !isLoaded,
                        child: Container(
                          margin: EdgeInsets.only(top: 50),
                          child: ClipOval(
                            child: Material(
                              color: Colors.blue, // button color
                              child: InkWell(
                                splashColor: Colors.green, // inkwell color
                                child: SizedBox(width: 256, height: 256, child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.search,size: 128,color: Colors.white,),
                                    Text("Cari Rental",style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold
                                    ),)
                                  ],
                                )),
                                onTap: () {
                                  setState(() {
                                    isLoaded = true;
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: isLoaded,
                        child: Container(
                        margin: EdgeInsets.only(left: 10,right: 10),
                        child: Column(
                          children: [
                            Container(
                              child: RaisedButton.icon(
                                icon: Icon(Icons.my_location),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                label: Text("GET CURRENT LOCATION"),
                                onPressed: () {
                                  getLocation();
                                },
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("1. Kapasitas Muatan"),
                                  Container(
                                    padding: EdgeInsets.all(5.0),
                                    child: DropdownButton<FuzzyModel>(
                                        value: _selectedItemSatu,
                                        items: _dropdownMenuItems,
                                        onChanged: (value) {
                                          setState(() {
                                            _selectedItemSatu = value;
                                          });
                                        }),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("2. Kapasitas Mesin"),
                                  Container(
                                    padding: EdgeInsets.all(5.0),
                                    child: DropdownButton<FuzzyModel>(
                                        value: _selectedItemDua,
                                        items: _dropdownMenuItems,
                                        onChanged: (value) {
                                          setState(() {
                                            _selectedItemDua = value;
                                          });
                                        }),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("3. Harga Sewa"),
                                  Container(
                                    padding: EdgeInsets.all(5.0),
                                    child: DropdownButton<FuzzyModel>(
                                        value: _selectedItemTiga,
                                        items: _dropdownMenuItems,
                                        onChanged: (value) {
                                          setState(() {
                                            _selectedItemTiga = value;
                                          });
                                        }),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("4. Tahun Kendaraan"),
                                  Container(
                                    padding: EdgeInsets.all(5.0),
                                    child: DropdownButton<FuzzyModel>(
                                        value: _selectedItemEmpat,
                                        items: _dropdownMenuItems,
                                        onChanged: (value) {
                                          setState(() {
                                            _selectedItemEmpat = value;
                                          });
                                        }),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("5. Jarak"),
                                  Container(
                                    padding: EdgeInsets.all(5.0),
                                    child: DropdownButton<FuzzyModel>(
                                        value: _selectedItemLima,
                                        items: _dropdownMenuItems,
                                        onChanged: (value) {
                                          setState(() {
                                            _selectedItemLima = value;
                                          });
                                        }),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                child: Text("SUBMIT"),
                                onPressed: () async {
                                  setState(() {
                                    result = [
                                      _selectedItemSatu.valueParam,
                                      _selectedItemDua.valueParam,
                                      _selectedItemTiga.valueParam,
                                      _selectedItemEmpat.valueParam,
                                      _selectedItemLima.valueParam
                                    ];
                                  });
                                  print(result);
                                  print('Latitude: $lat, Longitude: $long');
                                  if (lat != null && long != null) {
                                    final SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                    prefs.setString(
                                        Globals.fuzzyKey, json.encode(result));
                                    Navigator.of(context).push(
                                      MaterialPageRoute(builder: (_) {
                                        return ResultsScreen(result: result,isCreated: false,);
                                      }),
                                    );
                                  } else {
                                    CustomDialog.getDialog(
                                        "Peringatan",
                                        "Harap pencet button Get Current Location terlebih dahulu untuk mendapatkan lokasi terkini.",
                                        context);
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      ),)
                    ],
                  ),
                ),
              ));
  }
}
