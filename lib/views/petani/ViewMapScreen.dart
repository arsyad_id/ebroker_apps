// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:convert';
import 'dart:math';

import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:mapbox_search_flutter/mapbox_search_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

final LatLngBounds sydneyBounds = LatLngBounds(
  southwest: const LatLng(-34.022631, 150.620685),
  northeast: const LatLng(-33.571835, 151.325952),
);

class MapUiBody extends StatefulWidget {
  const MapUiBody();

  @override
  State<StatefulWidget> createState() => MapUiBodyState();
}

class MapUiBodyState extends State<MapUiBody> {
  MapUiBodyState();

  CameraPosition _kInitialPosition = CameraPosition(
    target: LatLng(-7.250445, 112.768845),
    zoom: 11.0,
  );
  MapboxMapController mapController;
  CameraPosition _position;
  bool _isMoving = false;
  bool _compassEnabled = true;
  CameraTargetBounds _cameraTargetBounds = CameraTargetBounds.unbounded;
  MinMaxZoomPreference _minMaxZoomPreference = MinMaxZoomPreference.unbounded;
  int _styleStringIndex = 0;
  // Style string can a reference to a local or remote resources.
  // On Android the raw JSON can also be passed via a styleString, on iOS this is not supported.
  List<String> _styleStrings = [
    MapboxStyles.MAPBOX_STREETS,
    MapboxStyles.SATELLITE,
    "assets/style.json"
  ];
  List<String> _styleStringLabels = [
    "MAPBOX_STREETS",
    "SATELLITE",
    "LOCAL_ASSET"
  ];
  bool _rotateGesturesEnabled = true;
  bool _scrollGesturesEnabled = true;
  bool _tiltGesturesEnabled = true;
  bool _zoomGesturesEnabled = true;
  bool _myLocationEnabled = true;
  bool _telemetryEnabled = true;
  MyLocationTrackingMode _myLocationTrackingMode = MyLocationTrackingMode.None;
  List<Object> _featureQueryFilter;
  Fill _selectedFill;
  double _currLat, _currLong;
  bool isLoading = false;
  double _selectedLat, _selectedLong;

  List<double> _listCoordinate = new List();

  @override
  void initState() {
    super.initState();
    getCurrLocation();
  }

  void getCurrLocation() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _currLat = prefs.getDouble(Globals.latKey);
    _currLong = prefs.getDouble(Globals.longKey);
    setState(() {
      _kInitialPosition = new CameraPosition(
        target: LatLng(_currLat, _currLong),
        zoom: 13.0,
      );
      _position = _kInitialPosition;
      isLoading = false;
    });
  }

  void _onMapChanged() {
    setState(() {
      _extractMapInfo();
    });
  }

  void _extractMapInfo() {
    _position = mapController.cameraPosition;
    _isMoving = mapController.isCameraMoving;
  }

  @override
  void dispose() {
    mapController.removeListener(_onMapChanged);
    super.dispose();
  }

  _clearFill() {
    if (_selectedFill != null) {
      mapController.removeFill(_selectedFill);
      setState(() {
        _selectedFill = null;
      });
    }
  }

  _drawFill(features) async {
    Map<String, dynamic> feature = jsonDecode(features[0]);
    if (feature['geometry']['type'] == 'Polygon') {
      var coordinates = feature['geometry']['coordinates'];
      List<List<LatLng>> geometry = coordinates
          .map(
              (ll) => ll.map((l) => LatLng(l[1], l[0])).toList().cast<LatLng>())
          .toList()
          .cast<List<LatLng>>();
      Fill fill = await mapController.addFill(FillOptions(
        geometry: geometry,
        fillColor: "#FF0000",
        fillOutlineColor: "#FF0000",
        fillOpacity: 0.6,
      ));
      setState(() {
        _selectedFill = fill;
      });
    }
  }

  static final LatLng center = const LatLng(-33.86711, 151.1947171);
  int _symbolCount = 0;

  SymbolOptions _getSymbolOptions(
      String iconImage, int symbolCount, double lat, double long) {
    LatLng geometry = LatLng(
      lat,
      long,
    );
    return iconImage == 'customFont'
        ? SymbolOptions(
            geometry: geometry,
            iconImage: 'airport-15',
            fontNames: ['DIN Offc Pro Bold', 'Arial Unicode MS Regular'],
            textField: 'Airport',
            textSize: 12.5,
            textOffset: Offset(0, 0.8),
            textAnchor: 'top',
            textColor: '#000000',
            textHaloBlur: 1,
            textHaloColor: '#ffffff',
            textHaloWidth: 0.8,
          )
        : SymbolOptions(
            geometry: geometry, iconImage: iconImage, iconSize: 1.5);
  }

  void _add(String iconImage, double lat, double long) {
    List<int> availableNumbers = Iterable<int>.generate(1).toList();
    mapController.symbols.forEach(
        (s) => availableNumbers.removeWhere((i) => i == s.data['count']));
    if (availableNumbers.isNotEmpty) {
      mapController.addSymbol(
          _getSymbolOptions(iconImage, availableNumbers.first, lat, long),
          {'count': availableNumbers.first});
      setState(() {
        _symbolCount += 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final MapboxMap mapboxMap = MapboxMap(
      accessToken: Globals.apiMapKey,
      onMapCreated: onMapCreated,
      initialCameraPosition: _kInitialPosition,
      // trackCameraPosition: true,
      // compassEnabled: _compassEnabled,
      // cameraTargetBounds: _cameraTargetBounds,
      // minMaxZoomPreference: _minMaxZoomPreference,
      // styleString: _styleStrings[_styleStringIndex],
      // rotateGesturesEnabled: _rotateGesturesEnabled,
      // scrollGesturesEnabled: _scrollGesturesEnabled,
      // tiltGesturesEnabled: _tiltGesturesEnabled,
      // zoomGesturesEnabled: _zoomGesturesEnabled,
      myLocationEnabled: _myLocationEnabled,
      // myLocationTrackingMode: _myLocationTrackingMode,
      // myLocationRenderMode: MyLocationRenderMode.GPS,
      onMapClick: (point, latLng) async {
        print(
            "Map click: ${point.x},${point.y}   ${latLng.latitude}/${latLng.longitude}");
        // print("Filter $_featureQueryFilter");
        /*List features = await mapController.queryRenderedFeatures(
            point, [], _featureQueryFilter);
        print('# features: ${features.length}');
        _clearFill();
        if (features.length == 0 && _featureQueryFilter != null) {
          Scaffold.of(context).showSnackBar(SnackBar(
              content: Text('QueryRenderedFeatures: No features found!')));
        } else {
          _drawFill(features);
        }*/
        final double distance = Geolocator.distanceBetween(Globals.latTugu,
            Globals.longTugu, latLng.latitude, latLng.longitude);
        print("JARAK : " + distance.toString());
        if (distance <= 50000) {
          setState(() {
            _selectedLat = latLng.latitude;
            _selectedLong = latLng.longitude;
          });
          _add("car-15", latLng.latitude, latLng.longitude);
        } else {
          CustomDialog.getDialog(
              "Peringatan",
              "Lokasi yang dipilih melebihi batas Gerbangkertosusila.",
              context);
        }
      },
      // onMapLongClick: (point, latLng) async {
      //   print(
      //       "Map long press: ${point.x},${point.y}   ${latLng.latitude}/${latLng.longitude}");
      //   Point convertedPoint = await mapController.toScreenLocation(latLng);
      //   LatLng convertedLatLng = await mapController.toLatLng(point);
      //   print(
      //       "Map long press converted: ${convertedPoint.x},${convertedPoint.y}   ${convertedLatLng.latitude}/${convertedLatLng.longitude}");
      //   /*double metersPerPixel =
      //   await mapController.getMetersPerPixelAtLatitude(latLng.latitude);
      //
      //   print(
      //       "Map long press The distance measured in meters at latitude ${latLng.latitude} is $metersPerPixel m");
      //
      //   List features =
      //   await mapController.queryRenderedFeatures(point, [], null);
      //   if (features.length > 0) {
      //     print(features[0]);
      //   }*/
      // },
      // onCameraTrackingDismissed: () {
      //   this.setState(() {
      //     _myLocationTrackingMode = MyLocationTrackingMode.None;
      //   });
      // },
      // onUserLocationUpdated: (location) {
      //   print(
      //       "new location: ${location.position}, alt.: ${location.altitude}, bearing: ${location.bearing}, speed: ${location.speed}, horiz. accuracy: ${location.horizontalAccuracy}, vert. accuracy: ${location.verticalAccuracy}");
      // },
    );
    return Scaffold(
        appBar: AppBar(
          title: Text("Pilih Lokasi"),
          centerTitle: true,
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: [
                  RaisedButton(
                    color: Colors.blue,
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Icon(
                            Icons.search,
                            color: Colors.white,
                          ),
                          Text(
                            "Search",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => buildSearchMap(),
                        ),
                      );
                    },
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.8,
                    child: mapboxMap,
                  )
                ],
              ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              _listCoordinate.add(_selectedLat);
              _listCoordinate.add(_selectedLong);
            });
            Navigator.pop(context, _listCoordinate);
          },
          child: Icon(
            Icons.check,
            color: Colors.white,
          ),
        ));
  }

  buildSearchMap() {
    return Scaffold(
        appBar: AppBar(
          title: Text("Cari Lokasi"),
          centerTitle: true,
        ),
        body: Container(
          padding: EdgeInsets.all(10),
          child: MapBoxPlaceSearchWidget(
            popOnSelect: true,
            apiKey: Globals.apiMapKey,
            searchHint: 'Cari lokasi ...',
            onSelected: (place) {
              double lat;
              double long;
              setState(() {
                long = place.center[0];
                lat = place.center[1];
              });
              print('Lat:$lat, Long: $long');
              // );
              mapController.moveCamera(
                CameraUpdate.newCameraPosition(
                  CameraPosition(
                    bearing: 270.0,
                    target: LatLng(lat, long),
                    tilt: 30.0,
                    zoom: 15.0,
                  ),
                ),
              );
            },
            context: context,
          ),
        ));
  }

  void onMapCreated(MapboxMapController controller) {
    mapController = controller;
    // mapController.addListener(_onMapChanged);
    /*_extractMapInfo();

    mapController.getTelemetryEnabled().then((isEnabled) => setState(() {
      _telemetryEnabled = isEnabled;
    }));*/
  }
}
