import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/TransactionScreen.dart';
import 'package:e_broker/views/credentials/LoginScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../AboutScreen.dart';
import 'SearchScreen.dart';

class HomeScreen extends StatefulWidget {
  int passIndex;
  HomeScreen({this.passIndex});
  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  String levelKey;

  List<Widget> _children = [SearchScreen(), TransactionScreen(), AboutScreen()];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  void initData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    levelKey = prefs.getString(Globals.levelKey);
    print('Level: $levelKey');
    // initBody();
  }

  void logout() async {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) {
      return LoginScreen();
    }), (Route<dynamic> route) => false);

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(Globals.isLoggedInKey);
    prefs.remove(Globals.idUserKey);
    prefs.remove(Globals.levelKey);
    prefs.remove(Globals.authKey);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(child: Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.car_rental),
            label: 'Order',
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.info),
            label: 'About',
          )
        ],
      ),
    ), onWillPop: () async => false);
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
