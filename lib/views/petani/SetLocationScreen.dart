import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:e_broker/models/RentalModel.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/ViewRouteScreen.dart';
import 'package:e_broker/views/petani/ViewMapScreen.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SetLocationScreen extends StatefulWidget {
  List<List<double>> fuzzySearch;
  SetLocationScreen({this.fuzzySearch});
  @override
  _SetLocationScreenState createState() => new _SetLocationScreenState();
}

class _SetLocationScreenState extends State<SetLocationScreen> {
  List<List<double>> result = new List();
  List<RentalModel> listRental = new List();
  bool isLoading = false;
  String authKey;

  double _currLat, _currLong;
  int _count = 4;
  List<TextEditingController> _kgPick = new List();
  List<TextEditingController> _addressPick = new List();
  List<double> _latPick = new List();
  List<double> _longPick = new List();
  List<RentalModel> _listLocation = new List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  void initData() async {
    setState(() {
      isLoading =true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    _currLat = prefs.getDouble(Globals.latKey);
    _currLong = prefs.getDouble(Globals.longKey);
    print(_currLat);
    print(_currLong);
    for (int i = 0; i < _count; i++) {
      setState(() {
        _kgPick.add(new TextEditingController());
        _addressPick.add(new TextEditingController());
        _latPick.add(0);
        _longPick.add(0);
      });
    }
    setState(() {
      _listLocation.add(new RentalModel.createTransaction(
          0,
          _currLong.toString(),
          _currLat.toString(),
          "lokasi petani"));
    });
    log(widget.fuzzySearch.toString(), name: "Fuzzy");
    setState(() {
      isLoading =false;
    });
  }

  void createTransaction() async {
    try {
      if (_listLocation.isNotEmpty) {
        setState(() {
          isLoading = true;
        });
        var options = Options(headers: {"Authorization": "Bearer " + authKey});
        var url = Globals.urlTransaction + "create";
        Map<String, dynamic> param = {'lokasi': _listLocation};
        String jsonParam = jsonEncode(param);
        print(jsonParam);
        Response response =
            await Dio().post(url, data: jsonParam, options: options);
        print(response.data);
        final data = response.data;
        if (data['status'] == 200) {
          log(widget.fuzzySearch.toString(), name: "Fuzzy");
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ViewRouteScreen(
                      idTransaction: data['data']['id'],
                      fuzzySearch: widget.fuzzySearch,
                      isCreated: true,
                    )),
          );
        }
      } else {
        CustomDialog.getDialog("Peringatan", "Harap input transaksi.", context);
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      print(e.response.data);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Input Transaksi"),
          centerTitle: true,
        ),
        floatingActionButton: RaisedButton(
          child: Text("Buat Transaksi"),
          onPressed: () {
            print(json.encode(_listLocation));
            createTransaction();
          },
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Container(
                child: Column(
                children: [buildCard()],
              )));
  }

  buildCard() {
    return Expanded(
        child: ListView.builder(
            itemCount: _count,
            itemBuilder: (context, index) {
              return Card(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: ListView(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    children: [
                      TextField(
                        controller: _kgPick[index],
                        decoration: InputDecoration(
                          labelText: "Input Berat (Kg)",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        keyboardType: TextInputType.number,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: TextField(
                          controller: _addressPick[index],
                          decoration: InputDecoration(
                            labelText: "Alamat Lengkap",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text("Latitude : " + _latPick[index].toString()),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child:
                            Text("Longitude : " + _longPick[index].toString()),
                      ),
                      _latPick[index] == 0 && _longPick[index] == 0
                          ? Container(
                              margin: EdgeInsets.only(top: 5),
                              child: RaisedButton.icon(
                                onPressed: () {
                                  if (_kgPick[index].text != "") {
                                    _navigateAndDisplaySelection(
                                        context, index);
                                  } else {
                                    CustomDialog.getDialog("Perhatian",
                                        "Harap input berat dahulu.", context);
                                  }
                                },
                                color: Colors.white,
                                shape: StadiumBorder(
                                    side: BorderSide(color: Colors.blue)),
                                icon: Icon(Icons.add_location_alt),
                                label: Text("Pilih Lokasi"),
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.only(top: 5),
                              child: RaisedButton(
                                onPressed: null,
                                color: Colors.white,
                                shape: StadiumBorder(
                                    side: BorderSide(color: Colors.blue)),
                                child: Text("Lokasi telah dipilih"),
                              ),
                            )
                    ],
                  ),
                ),
              );
            }));
  }

  _navigateAndDisplaySelection(BuildContext context, int index) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MapUiBody()),
    );

    print(result);

    if (result != null){
      setState(() {
        _latPick[index] = result[0];
        _longPick[index] = result[1];
        _listLocation.add(new RentalModel.createTransaction(
            int.parse(_kgPick[index].text),
            _longPick[index].toString(),
            _latPick[index].toString(),
            _addressPick[index].text));
      });
    }else{
      CustomDialog.getDialog("Peringatan", "Lokasi belum dipilih.", context);
    }
  }
}
