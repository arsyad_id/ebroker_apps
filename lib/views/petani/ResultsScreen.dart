import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:e_broker/models/RentalModel.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/petani/HomeScreen.dart';
import 'package:e_broker/views/petani/SetLocationScreen.dart';
import 'package:e_broker/views/petani/ViewMapScreen.dart';
import 'package:e_broker/views/rental/DetailRentalScreen.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ResultsScreen extends StatefulWidget {
  List<List<double>> result;
  bool isCreated;
  int idTrans;
  var fuzzy;

  ResultsScreen({this.result, this.isCreated, this.fuzzy, this.idTrans});

  @override
  _ResultsScreenState createState() => new _ResultsScreenState();
}

class _ResultsScreenState extends State<ResultsScreen> {
  List<List<double>> result = new List();
  List<RentalModel> listRental = new List();
  bool isLoading = false;
  String authKey;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  void initData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    print(widget.result);
    searchFuzzy();
  }

  void searchFuzzy() async {
    try {
      if (widget.result != null) {
        setState(() {
          isLoading = true;
        });
        var url = Globals.urlRoot + "fuzzy-topsis";
        Map<String, dynamic> param = new Map();
        param['bobot'] = widget.result;
        var jsonParam = json.encode(param);
        var options = Options(headers: {"Authorization": "Bearer " + authKey});
        Response response =
            await Dio().post(url, data: jsonParam, options: options);
        print(response.data);
        final data = response.data;
        if (data != null) {
          if (data['status'] == 200) {
            print("OK");
            List _dataResults = data['data']['rangking'];
            print(_dataResults);
            _dataResults.forEach((element) {
              listRental.add(new RentalModel(
                  element['id'],
                  element['id_user'],
                  element['nama'],
                  element['jenis_truk'],
                  element['foto'],
                  element['alamat'],
                  element['long'],
                  element['lat'],
                  element['created_at'],
                  element['updated_at'],
                  element['nilaiPreferensi'],
                  element['rating'] != null
                      ? double.parse(element['rating'].toString())
                      : 0));
            });
          } else {
            CustomDialog.getDialog("Peringatan", data['message'], context);
          }
        }
      } else {
        CustomDialog.getDialog("Peringatan", "Bobot tidak valid.", context);
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  void createTransaction(int idRental) async {
    try {
      if (widget.result != null) {
        setState(() {
          isLoading = true;
        });
        var url = Globals.urlTransaction + "create-2";
        Map<String, dynamic> param = new Map();
        param['id_transaksi'] = widget.idTrans;
        param['id_rental'] = idRental;
        var jsonParam = json.encode(param);
        var options = Options(headers: {"Authorization": "Bearer " + authKey});
        Response response =
            await Dio().post(url, data: jsonParam, options: options);
        print(response.data);
        final data = response.data;
        if (data != null) {
          if (data['status'] == 200) {
            print("OK");
            Navigator.of(context)
                .pushReplacement(MaterialPageRoute(builder: (_) {
              return HomeScreen();
            }));
            CustomDialog.getDialog(
                "Sukses",
                "Transaksi berhasil dibuat, silahkan tunggu konfirmasi pihak rental.",
                context);
          } else {
            CustomDialog.getDialog("Peringatan", data['message'], context);
          }
        }
      } else {
        CustomDialog.getDialog("Peringatan", "Bobot tidak valid.", context);
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Result"),
          centerTitle: true,
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : buildList());
  }

  buildList() {
    return Container(
        child: Column(
      children: [
        Expanded(
          child: ListView.builder(
              itemCount: listRental.length,
              itemBuilder: (context, index) {
                // double rate = double.parse(listRental[index].rate);
                return Card(
                  child: ExpansionTile(
                    initiallyExpanded: true,
                    title: Text(listRental[index].name ?? "-"),
                    subtitle: Text(listRental[index].truck ?? "-"),
                    trailing: RatingBarIndicator(
                      rating: double.parse(listRental[index].rate.toString()),
                      itemBuilder: (context, index) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      itemCount: 5,
                      itemSize: 20.0,
                      direction: Axis.horizontal,
                    ),
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Nilai Preferensi"),
                              Text(listRental[index].prefsValue.toString() ??
                                  "-"),
                            ],
                          )),
                      Container(
                          margin: EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Alamat Rental"),
                              Text(listRental[index].address ?? "-"),
                            ],
                          )),
                      widget.isCreated
                          ? Container(
                              margin: EdgeInsets.only(bottom: 5),
                              width: MediaQuery.of(context).size.width,
                              child: RaisedButton(
                                shape: StadiumBorder(),
                                padding: EdgeInsets.all(15),
                                color: Colors.blue,
                                child: Text(
                                  "PILIH RENTAL",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () {
                                  showAlertDialog(
                                      context, listRental[index].id);
                                },
                              ),
                            )
                          : Container(),
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        width: MediaQuery.of(context).size.width,
                        child: RaisedButton(
                          shape: StadiumBorder(),
                          padding: EdgeInsets.all(15),
                          color: Colors.blueGrey,
                          child: Text(
                            "DETAIL RENTAL",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                              return DetailRentalScreen(
                                idRental: listRental[index].id,
                              );
                            }));
                          },
                        ),
                      )
                    ],
                  ),
                );
              }),
        ),
        widget.isCreated
            ? Container()
            : Container(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  padding: EdgeInsets.all(15),
                  color: Colors.blue,
                  child: Text(
                    "Lanjut",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                      return SetLocationScreen(
                        fuzzySearch: widget.result,
                      );
                    }));
                  },
                ),
              ),
      ],
    ));
  }

  showAlertDialog(BuildContext context, int _id) {
    // set up the buttons
    Widget cancelButton = MaterialButton(
      child: Text(
        "TIDAK",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = MaterialButton(
      child: Text(
        "YA",
        style: TextStyle(color: Colors.blue),
      ),
      onPressed: () {
        createTransaction(_id);
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Peringatan"),
      content: Text("Apakah Anda yakin ingin memilih rental ini?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
