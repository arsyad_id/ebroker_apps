import 'package:e_broker/views/petani/HomeScreen.dart';
import 'package:flutter/material.dart';

class MenuScreen extends StatefulWidget {
  @override
  _MenuScreenState createState() => new _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            SafeArea(
              child: Container(
                padding: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width,
                color: Colors.lightBlue,
                child: Row(
                  children: [
                    Icon(Icons.person, color: Colors.white,),
                    Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Hi, Petani 1", style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                          ),),
                          Text("petani1@mail.com",style: TextStyle(
                              color: Colors.white,
                              fontSize: 16
                          ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.all(10),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)
                ),
                color: Colors.white38,
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Column(
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: IconButton(
                                icon: Icon(Icons.search,size: 64,),
                                onPressed: (){
                                  Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(builder: (_) {
                                      return HomeScreen();
                                    }),
                                  );
                                },
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text("Cari Rental",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold
                              ),),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.only(top: 10, bottom: 20),
                        child: Column(
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: IconButton(
                                icon: Icon(Icons.shopping_cart,size: 64,),
                                onPressed: (){},
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text("Transaksi",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold
                              ),),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}