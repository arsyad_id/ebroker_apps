import 'package:dio/dio.dart';
import 'package:e_broker/models/RentalModel.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailTransactionScreen extends StatefulWidget {
  int idTrans;
  DetailTransactionScreen({this.idTrans});
  @override
  _DetailTransactionScreenState createState() => new _DetailTransactionScreenState();
}

class _DetailTransactionScreenState extends State<DetailTransactionScreen> {
  bool isLoading = false;
  String authKey, level, email;
  List<RentalModel> _listDetail = new List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  initData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    email = prefs.getString(Globals.emailKey);
    level = prefs.getString(Globals.levelKey);
    loadDetailTransaction();
  }

  void loadDetailTransaction() async {
    try {
      _listDetail.clear();
      var url = Globals.urlTransaction + "view/" + widget.idTrans.toString();
      var options = Options(headers: {"Authorization": "Bearer " + authKey});
      Response response = await Dio().get(url, options: options);
      print(response.data);
      final data = response.data;
      if (data != null) {
        if (data['status'] == 200) {
          print("OK");
          List _dataResults = data['data']['location'];
          print(_dataResults);
          _dataResults.forEach((element) {
            _listDetail.add(new RentalModel.createTransaction(
                int.parse(element['kg_pick']),
                element['longitude'],
                element['latitude'],
                element['alamat']));
          });
        } else {
          CustomDialog.getDialog("Peringatan", data['message'], context);
        }
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Transaksi"),
        automaticallyImplyLeading: true,
      ),
      body: isLoading ? Center(
        child: CircularProgressIndicator(),
      )
      :Container(
        child: Column(
          children: [
            Container(
              child: Expanded(
                child: buildCard(),
              )
            )
          ],
        ),
      ),
    );
  }

  buildCard(){
    return ListView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      itemCount: _listDetail.length,
        itemBuilder: (context, index){
      return Card(
        child: ListView(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.blueAccent,
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(bottom: 10),
              alignment: Alignment.center,
              child: Text("Lokasi ke-"+(index+1).toString(),style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.bold
              ),),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Longitude"),
                  Text(_listDetail[index].long)
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Latitude"),
                  Text(_listDetail[index].lat)
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Alamat Lokasi"),
                  Text(_listDetail[index].address)
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Berat Kiriman"),
                  Text(_listDetail[index].kgPick.toString())
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}