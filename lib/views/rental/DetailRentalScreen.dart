import 'package:dio/dio.dart';
import 'package:e_broker/models/RentalModel.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailRentalScreen extends StatefulWidget {
  int idRental;
  DetailRentalScreen({this.idRental});
  @override
  _DetailRentalScreenState createState() => new _DetailRentalScreenState();
}

class _DetailRentalScreenState extends State<DetailRentalScreen> {
  RentalModel rentalModel;
  bool isLoading = false;
  String authKey, email, level;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  void initData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    email = prefs.getString(Globals.emailKey);
    level = prefs.getString(Globals.levelKey);
    loadDetail();
  }

  void loadDetail() async {
    try {
      print("ID: " + widget.idRental.toString());
      var url = Globals.urlRental + widget.idRental.toString();
      var options = Options(headers: {"Authorization": "Bearer " + authKey});
      Response response = await Dio().get(url, options: options);
      print(response.data);
      final data = response.data;
      if (data != null) {
        if (data['status'] == 200) {
          print("OK");
          final _dataResults = data['data'];
          print(_dataResults);
          rentalModel = new RentalModel.detailRental(
            _dataResults['id'],
            _dataResults['id_user'],
            _dataResults['nama'],
            _dataResults['jenis_truk'],
            _dataResults['foto'],
            _dataResults['alamat'],
            _dataResults['long'],
            _dataResults['lat'],
            _dataResults['created_at'],
            _dataResults['updated_at'],
            _dataResults['rental_kriteria'][4]['input_nilai'],
            _dataResults['rental_kriteria'][0]['input_nilai'],
            _dataResults['rental_kriteria'][1]['input_nilai'],
            _dataResults['rental_kriteria'][2]['input_nilai'],
            _dataResults['rental_kriteria'][3]['input_nilai'],
          );
        } else {
          CustomDialog.getDialog("Peringatan", data['message'], context);
        }
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Rental"),
        automaticallyImplyLeading: true,
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(16)),
                      width: 350,
                      height: 250,
                      child: rentalModel.photos == null
                          ? Icon(
                              Icons.image,
                              size: 128,
                            )
                          : Image.network(Globals.urlRoot + rentalModel.photos)),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [Text("Nama"), Text(rentalModel.name ?? "-")],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Jenis Truk"),
                        Text(rentalModel.truck ?? "-")
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Alamat"),
                        Text(rentalModel.address ?? "-")
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Jarak"),
                        Text(rentalModel.distance + " Km" ?? "-")
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Muatan"),
                        Text(rentalModel.capacityContainer + " TON" ?? "-")
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Mesin"),
                        Text(rentalModel.capacityMachine + " CC" ?? "-")
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Tahun"),
                        Text(rentalModel.carYear ?? "-")
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Harga Sewa"),
                        Text("Rp. " + rentalModel.rentPrice ?? "-")
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
