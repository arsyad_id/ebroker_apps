import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:e_broker/models/RentalModel.dart';
import 'package:e_broker/utils/Globals.dart';
import 'package:e_broker/views/ViewRouteScreen.dart';
import 'package:e_broker/views/petani/ViewMapScreen.dart';
import 'package:e_broker/views/rental/DetailTransactionScreen.dart';
import 'package:e_broker/widgets/CustomDialog.dart';
import 'package:e_broker/widgets/CustomHeader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RentalScreen extends StatefulWidget {
  @override
  _RentalScreenState createState() => new _RentalScreenState();
}

class _RentalScreenState extends State<RentalScreen> {
  List<List<double>> result = new List();
  List<RentalModel> listTransaction = new List();
  bool isLoading = false;
  String authKey, email, level;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  void initData() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    authKey = prefs.getString(Globals.authKey);
    email = prefs.getString(Globals.emailKey);
    level = prefs.getString(Globals.levelKey);
    loadListTransaction();
  }

  void loadListTransaction() async {
    try {
      listTransaction.clear();
      var url = Globals.urlTransaction + "list";
      var options = Options(headers: {"Authorization": "Bearer " + authKey});
      Response response = await Dio().get(url, options: options);
      print(response.data);
      final data = response.data;
      if (data != null) {
        if (data['status'] == 200) {
          print("OK");
          List _dataResults = data['data'];
          print(_dataResults);
          _dataResults.forEach((element) {
            listTransaction.add(new RentalModel.listTransaction(
                element['id'],
                element['created_at'],
                element['updated_at'],
                element['id_petani'],
                element['id_rental'],
                element['status']));
          });
        } else {
          CustomDialog.getDialog("Peringatan", data['message'], context);
        }
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
  }

  void accOrder(int _idTransaction) async {
    try {
      setState(() {
        isLoading = true;
      });
      var url = Globals.urlTransaction + "accept/" + _idTransaction.toString();
      var options = Options(headers: {"Authorization": "Bearer " + authKey});
      Response response = await Dio().get(url, options: options);
      print(response.data);
      final data = response.data;
      if (data['status'] == 200) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ViewRouteScreen(
                idTransaction: _idTransaction,
                isCreated: false,
              )),
        );
        CustomDialog.getDialog(
            data['message'], "Transaksi berhasil diproses.", context);
      }
      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response.statusMessage);
      print(e.response.data);
      CustomDialog.getDialog(
          e.response.statusMessage, e.response.data.toString(), context);
    }
    setState(() {
      isLoading = false;
    });
    loadListTransaction();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : buildList());
  }

  buildList() {
    return Container(
        child: Column(
          children: [
            CustomHeader.getHeaderRental(context, level, email),
            Container(
              margin: EdgeInsets.only(top: 5, bottom: 5),
              child: Image.asset('assets/TA banner-01 hijau.jpg'),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text("DAFTAR TRANSAKSI",style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20
              ),),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: listTransaction.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListView(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        children: [
                          Container(
                              margin: EdgeInsets.all(15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text((index+1).toString()+". " + "Status Pesanan"),
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        color: listTransaction[index].status ==
                                            "Diproses"
                                            ? Colors.green
                                            : listTransaction[index].status ==
                                            "Menunggu"
                                            ? Colors.yellow
                                            : Colors.blue,
                                        borderRadius: BorderRadius.circular(15)),
                                    child: Text(
                                      listTransaction[index].status,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                  ),
                                ],
                              )),
                          Container(
                              margin: EdgeInsets.all(15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Pesanan dibuat"),
                                  Text(DateTime.parse(listTransaction[index].createdAt).toString()),
                                ],
                              )),
                          listTransaction[index].status == "Menunggu"
                              ? Container(
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: RaisedButton(
                              shape: StadiumBorder(),
                              color: Colors.blue,
                              // shape: StadiumBorder(),
                              onPressed: () {
                                showAlertDialog(
                                    context, listTransaction[index].id);
                              },
                              child: Text(
                                "TERIMA ORDER",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                              : Container(),
                          listTransaction[index].status == "Diproses"
                              ? Container(
                            margin: EdgeInsets.only(
                                left: 20, right: 20, bottom: 10),
                            child: MaterialButton(
                              color: Colors.teal,
                              shape: StadiumBorder(),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                                  return ViewRouteScreen(idTransaction: listTransaction[index].id, isCreated: false,);
                                }));
                              },
                              child: Text(
                                "LIHAT RUTE",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                              : Container(),
                          Container(
                            margin: EdgeInsets.only(
                                left: 20, right: 20, bottom: 10),
                            child: MaterialButton(
                              color: Colors.blueGrey,
                              shape: StadiumBorder(),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                                  return DetailTransactionScreen(idTrans: listTransaction[index].id);
                                }));
                              },
                              child: Text(
                                "DETAIL TRANSAKSI",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
            ),
          ],
        ));
  }

  showAlertDialog(BuildContext context, int _id) {
    // set up the buttons
    Widget cancelButton = MaterialButton(
      child: Text(
        "TIDAK",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () {},
    );
    Widget continueButton = MaterialButton(
      child: Text(
        "YA",
        style: TextStyle(color: Colors.blue),
      ),
      onPressed: () {
        accOrder(_id);
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Peringatan"),
      content: Text("Apakah Anda yakin ingin memproses transaksi ini?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showRatingBar(BuildContext context, String _id) {
    // set up the buttons
    Widget continueButton = MaterialButton(
      child: Text(
        "SUBMIT",
        style: TextStyle(color: Colors.blue),
      ),
      onPressed: () {
        // finishOrder(_id);
        Navigator.pop(context);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Beri Rating"),
      content: RatingBar.builder(
        initialRating: 1,
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: false,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Colors.amber,
        ),
        onRatingUpdate: (rating) {
          print(rating);
        },
      ),
      actions: [
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
