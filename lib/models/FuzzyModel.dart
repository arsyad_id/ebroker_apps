class FuzzyModel {
  String parameter;
  List<double> valueParam;

  FuzzyModel(this.valueParam, this.parameter);

  @override
  String toString() {
    return 'FuzzyModel{parameter: $parameter, valueParam: $valueParam}';
  }
}