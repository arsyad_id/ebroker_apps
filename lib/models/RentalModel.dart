class RentalModel {
  int _id, _kgPick;
  double _prefsValue, _rate;
  String _userId,
      _name,
      _truck,
      _photos,
      _address,
      _long,
      _lat,
      _createdAt,
      _updatedAt,
      _idPetani,
      _idRental,
      _status,
      _distance,
      _capacityContainer,
      _capacityMachine,
      _rentPrice,
      _carYear;

  RentalModel(
      this._id,
      this._userId,
      this._name,
      this._truck,
      this._photos,
      this._address,
      this._long,
      this._lat,
      this._createdAt,
      this._updatedAt,
      this._prefsValue,
      this._rate);

  RentalModel.detailRental(
      this._id,
      this._userId,
      this._name,
      this._truck,
      this._photos,
      this._address,
      this._long,
      this._lat,
      this._createdAt,
      this._updatedAt,
      this._distance,
      this._capacityContainer,
      this._capacityMachine,
      this._rentPrice,
      this._carYear);

  RentalModel.listTransaction(this._id, this._createdAt, this._updatedAt,
      this._idPetani, this._idRental, this._status);

  RentalModel.createTransaction(
      this._kgPick, this._long, this._lat, this._address);

  get distance => _distance;

  set distance(value) {
    _distance = value;
  }

  get kgPick => _kgPick;

  set kgPick(value) {
    _kgPick = value;
  }

  get idPetani => _idPetani;

  set idPetani(value) {
    _idPetani = value;
  }

  double get prefsValue => _prefsValue;

  set prefsValue(double value) {
    _prefsValue = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get userId => _userId;

  get updatedAt => _updatedAt;

  set updatedAt(value) {
    _updatedAt = value;
  }

  get createdAt => _createdAt;

  set createdAt(value) {
    _createdAt = value;
  }

  get lat => _lat;

  set lat(value) {
    _lat = value;
  }

  get long => _long;

  set long(value) {
    _long = value;
  }

  get address => _address;

  set address(value) {
    _address = value;
  }

  get photos => _photos;

  set photos(value) {
    _photos = value;
  }

  get truck => _truck;

  set truck(value) {
    _truck = value;
  }

  get name => _name;

  set name(value) {
    _name = value;
  }

  set userId(String value) {
    _userId = value;
  }

  get rate => _rate;

  set rate(value) {
    _rate = value;
  }

  get idRental => _idRental;

  set idRental(value) {
    _idRental = value;
  }

  get status => _status;

  set status(value) {
    _status = value;
  }

  Map<String, dynamic> toJson() {
    return {
      "kg_pick": this._kgPick,
      "longitude": this._long,
      "latitude": this._lat,
      "alamat": this._address
    };
  }

  @override
  String toString() {
    return 'RentalModel{_kgPick: $_kgPick, _long: $_long, _lat: $_lat}';
  }

  get capacityContainer => _capacityContainer;

  set capacityContainer(value) {
    _capacityContainer = value;
  }

  get capacityMachine => _capacityMachine;

  set capacityMachine(value) {
    _capacityMachine = value;
  }

  get rentPrice => _rentPrice;

  set rentPrice(value) {
    _rentPrice = value;
  }

  get carYear => _carYear;

  set carYear(value) {
    _carYear = value;
  }
}
